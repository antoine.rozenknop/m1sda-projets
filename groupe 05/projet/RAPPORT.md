## Membres:

    Noms Prenoms:
    KANE Mamadou 12014527
    FOMO DAFANG Rykiel Galina 12208940
    MAZNA Gnouyadou Romaric 12212020

## Objectif du projet:

    L'objectif est de proposer une représentation en 3D ou 2D d'un graphe en partant d'une description au format .dot de ce graphe (liste de nœuds et d'arêtes).

## Démarches:

    Pour pouvoir traiter le sujet. Nous avons commencer par faire une visualisation de graphe avec une méthode naïve de calcul de force. Puis nous avons implémenté un Quadtree qui permet de simplifier les calculs de forces.

### Méthode naïve :

    *   Pour chaque noeud, nous avons généré aléatoirement une coordonné (x,y)
    *  Puis nous avons défini les forces d'attraction et de répulsion de la maniére suivante

    '''python
    def forceAttractive(n1,n2,positions):
        return math.sqrt((positions[n2][0] - positions[n1][0])**2 + (positions[n2][1] - positions[n1][1])**2)
    '''

    '''python
    def forceRepulsive(n1,n2,positions,k=0.1):
        dist = forceAttractive(n1,n2,positions)
        aide = (nx.degree(G,nbunch=n1) + 1) * (nx.degree(G,nbunch=n2) + 1)
        return k * (aide/dist)
    '''

    * Après application des forces nous obtenons de nouvelles         coordonnées qui sont calculées de la maniére suivante :
        F = m*a ==> a = F / m
        v_new = v_old + a*dt
        v_old = 0 donc v_new = a*dt
        x_new = x_old + v_new*dt
        y_new = y_old + v_new*dt
        a : acceleration | m : masse = degree | F : force | v_new : nouvelle vitesse
        x_new : nouvelle coordonnée x | dt : temps

    * Ces forces seront appliquées aux noeuds un nombre n=10000 fois
    * Puis nous visualisons le graphe avant et après application des forces.
    * Nous voyons nettement que la visualisation est meilleure après application des forces.

### Avec les QuadTrees:

    Avec un Quadtree les calculs deviennent plus simple. Si un groupe de noeuds est éloigné d'un noeud donné, il n'agira que comme un seul noeud qui sera le noeud racine dans le rectangle considéré. Ceci nous permet de gagner en temps de calcul.
