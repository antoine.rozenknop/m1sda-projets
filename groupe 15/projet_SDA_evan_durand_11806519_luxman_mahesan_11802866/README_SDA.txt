Evan DURAND 11806519
Luxman MAHESAN 11802866


Notre code fonctionne sur python 3.7.0 et il faut installer les bibliothèques suivante:

-networkx avec la commande suivante: pip install networkx
-numpy avec la commande suivante: pip install numpy
-matplotlib avec la commande suivante: pip install matplotlib
-pydot avec la commande suivante: pip install pydot
-psutil avec la commande suivante: pip install psutil


Pour executer notre code il faut: 

-ouvrir un terminal dans le répertoire fa2l-master
-entrez la commande : python bla.py
-entrez le nom d'un fichier dot qui est dans le répertoire fa2l-master ( CH4.dot )
-entrez le nombre d'itérations que l'algorithme ForceAtlas doit réaliser