import networkx as nx
from fa2l import force_atlas2_layout
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import read_dot
import time
import psutil

fichier = input("Entrez le nom d'un fichier dot : ")
iter = int(input("Entrez le nombre d'itérations : "))


G = nx.Graph(nx.nx_pydot.read_dot(fichier))

#G = nx.erdos_renyi_graph(100, 0.15, directed=False)

mem_avant = psutil.virtual_memory().used
debut = time.time()

positions = force_atlas2_layout(G,
                                iterations=iter,
                                pos_list=None,
                                node_masses=None,
                                outbound_attraction_distribution=False,
                                lin_log_mode=False,
                                prevent_overlapping=False,
                                edge_weight_influence=1.0,

                                jitter_tolerance=1.0,
                                barnes_hut_optimize=True,
                                barnes_hut_theta=1.5,

                                scaling_ratio=2.0,
                                strong_gravity_mode=False,
                                multithread=False,
                                gravity=1.0)

fin = time.time()
temps_execution = fin - debut
print("Temps d'exécution :", temps_execution)

mem_apres = psutil.virtual_memory().used
mem_utilisee = mem_apres - mem_avant
print("Mémoire utilisée :", mem_utilisee)

nx.draw_networkx(G, positions, cmap=plt.get_cmap('jet'), node_size=50, with_labels=False)
plt.show()
