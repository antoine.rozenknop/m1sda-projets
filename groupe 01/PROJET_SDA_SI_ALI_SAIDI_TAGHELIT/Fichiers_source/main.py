import Octree
import Octree as TREE
import random
Omega=0.5
import numpy as np
OctTree=TREE.Octree(-50,-50,-50,50,50,50)
class Point():


    def __init__(self, x, y, z, name):

        self.X = x
        self.Y = y
        self.Z = z
        self.name=name

    def __str__(self):
        return "Point(%s,%s,%s, %s)" % (self.X, self.Y, self.Z,self.name)

    def getX(self):
        return self.X

    def getY(self):
        return self.Y
    def getZ(self):
        return self.Z
    def getname(self):
        return self.name


import networkx as nx
G = nx.Graph(nx.nx_pydot.read_dot('H2O.dot'))

x=0
y=0
z=0
Points=[]
for i in list(G.nodes):
    Points.append(Point(x,y,z,i))
    x=x+1
    y=y+1
    z=z+1

print(Points[0])

#calcul du vecteur unitaire
def get_unit_vector(pv,pu):
  R = pu-pv
  V = np.sqrt(pv[0]**2+pv[1]**2+pv[2]**2)
  return R/V
#calcul de la distance euclidienne
def Euclidean_distance(pv,pu):
  R = (pv-pu)**2
  R = np.sqrt(np.sum(R))
  return R

#calcule de la force répulsive
def Frep(l,pv,pu):
  pu = np.array([pu.getX(), pu.getY(), pu.getZ()])
  pv = np.array([pv.getX(), pv.getY(), pv.getZ()])
  R = ((l**2)/Euclidean_distance(pv,pu))*get_unit_vector(pv,pu)
  return R

#calcul de la force attractive
def Fattr(l,pv,pu):
  pu = np.array([pu.getX(), pu.getY(), pu.getZ()])
  pv = np.array([pv.getX(), pv.getY(), pv.getZ()])
  R = (Euclidean_distance(pv,pu)**2)/l
  R*= get_unit_vector(pu,pv)
  return R

#chercher un point dans la liste de points
def search_point(Points,name):
  for point_position in range(len(Points)):
    if Points[point_position].getname() == name:
      return Points[point_position]


def COM(T,poid):
    o = TREE.Octpoint(0, 0, 0)
    if (hasattr(T, 'children')):
        for i in T.children:
            if (i.Point == None):
                b = COM(i,1)
                o.setX(o.getX() + b.getX())
                o.setY(o.getY() + b.getY())
                o.setZ(o.getZ() + b.getZ())

            elif (not i.Point.isnullified()):
                o.setX(o.getX() + i.Point.getX())
                o.setY(o.getY() + i.Point.getY())
                o.setZ(o.getZ() + i.Point.getZ())
    o.setX(o.getX() / poid)
    o.setY(o.getY() / poid)
    o.setZ(o.getZ() / poid)

    return o


def parcour_anti_grave(T :Octree.Octree ,Coo,l) -> float:
    O=0
    if (hasattr(T, 'children')):

        for i in T.children:

            if (i.Point == None):

                if(i.poid>1):
                    Centre_Gravité_1 = COM(i,i.poid)
                    if(i.poid/Euclidean_distance(np.array([Centre_Gravité_1.getX(),Centre_Gravité_1.getY(),Centre_Gravité_1.getZ()]),
                                                 np.array([Coo.getX(),Coo.getY(),Coo.getZ()])<Omega)):



                        return O+ Frep(l, Coo, Centre_Gravité_1)


                    else: O+ parcour_anti_grave(i  ,Coo,l)
                elif(not i.Point.isnullified):

                        O=O+ Frep(l, Coo, np.array(
                            [i.Point.getX(), i.Point.getY, i.Point.getZ]))

        return O
    else: return O

#calcul des coordonnées finales des points

def displacement_vector(sigma, l, graph, Points):
    New_coord = []
    all_forces = []
    Max_force = 0.
    for point_position in range(len(Points)):

        voisins_U = list(graph.neighbors(Points[point_position].getname()))

        force_attr = np.array([0., 0., 0.])
        force_rep = np.array([0., 0., 0.])
        force_final = np.array([0., 0., 0.])
        # U est notre point
        U = Points[point_position]

        for i in range(len(Points)):

            if (U.getname() != Points[i].getname()):
                force_rep += Frep(l, U, Points[i])

        ''' force_rep=parcour_anti_grave(Octree,U,l)  peutetre utiliser pour simplifier le calcule des force repulssive avec l'octree
           
           '''
        # on ne prend que les link de U
        for voisin_position in range(len(voisins_U)):
            # on cherche les coordonnées de V dans notre liste
            name_V = voisins_U[voisin_position]
            V = search_point(Points, name_V)
            force_attr += Fattr(l, U, V)
            # force_rep  += Frep(l,U,V)
            voisin_position += 1
        force_final = force_attr + force_rep
        if np.abs(
                sum(force_final)) > Max_force:  # on prend la valeur absolue car -6 > 2 alors qu'il déplace plus le point.
            Max_force = np.abs(sum(force_final))
        U_array = np.array([U.getX(), U.getY(), U.getZ()])

        U_new = U_array + sigma * force_final

        New_coord.append(Point(U_new[0], U_new[1], U_new[2], U.getname()))
        all_forces.append(force_final)
    return New_coord, Max_force

Coord = []

for p in list(G.nodes):
    Coord.append(Point(random.uniform(-20, 20),random.uniform(-20, 20),random.uniform(-20, 20),p))



for i in range(200):
#ici sigma = 0.05 et l = 30
  New_Coord,Max_Force = displacement_vector(0.05,30,G,Coord)
  if Max_Force < 0.5:
    break
  else:
    Coord = New_Coord
    OctTree=None
    OctTree=OctTree=TREE.Octree(-200,-200,-200,200,200,200)

    for m in Coord:
         OctTree.insert(m.getX(), m.getY(), m.getZ(), m.getname())

'''for i in range(len(Coord)):
  print(Coord[i])

'''
import matplotlib.pyplot as plt
import numpy as np

# Fixing random state for reproducibility
np.random.seed(19680801)



def randrange(n, vmin, vmax):
    """
    Helper function to make an array of random numbers having shape (n, )
    with each number distributed Uniform(vmin, vmax).
    """
    return (vmax - vmin)*np.random.rand(n) + vmin

fig = plt.figure()
ax = fig.add_subplot(projection='3d')



def ajt_point(T: Octree.Octree):
    if(hasattr(T,'children')):
        for i in T.children:
            if (i.Point == None):
                ajt_point(i);
            elif (not i.Point.isnullified()):
                ax.scatter( i.Point.getX() , i.Point.getY() , i.Point.getZ(), c='red')











def Cube(T: Octree.Octree):
    if(hasattr(T,'children')):
        for i in T.children:
            if (i.Point == None):
                Cube(i);

                ax.scatter(i.topLeftFront.getX(),i.topLeftFront.getY(), i.topLeftFront.getZ(),c='black')
                ax.scatter(i.bottomRightBack.getX(), i.bottomRightBack.getY(), i.bottomRightBack.getZ(), c='black')




ajt_point(OctTree)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
for i in range(len(Coord)):

    for j in range(len(Coord)):
        if (Coord[j].getname() in G.neighbors(Coord[i].getname())):
            ax.plot([Coord[j].getX(), Coord[i].getX()],[Coord[j].getY(), Coord[i].getY()],[Coord[j].getZ(), Coord[i].getZ()],c='blue')

plt.show()


