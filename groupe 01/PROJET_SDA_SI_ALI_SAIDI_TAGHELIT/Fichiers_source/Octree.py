#structure point
class Octpoint():
    nullify = False;
    def __init__(self, *arg):

       if(len(arg)==3):

            self.x = arg[0]
            self.y = arg[1]
            self.z = arg[2]
       else:
           self.nullify = True;


    def __str__(self):
        return "Point(%s,%s,%s)" % (self.x, self.y, self.z)



    def isnullified(self):
        return self.nullify
    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getZ(self):
        return self.z

    def setX(self, x):
        self.x = x

    def setY(self, y):
        self.y = y

    def setZ(self, z):
        self.z = z

#classe pour un noeud de l'octree
class Noeud():

    def __init__(self, label, name):

        self.label = label
        self.name = name

    def Get_Label(self):
        return self.label

    def Get_Noeud(self):
        return self.Noeud



    def Get_Name(self):
        return self.name

    def set_Label(self, label):
        self.label = label

    def set_Name(self, name):
        self.name = name




#la classe principale, elle contient toute les méthodes de manipulation de notre octree et notre structure octree.
class Octree():
    poid=0

    topLeftFront =Octpoint()
    bottomRightBack=Octpoint()

    object=None
    def __init__(self, *arg):
        if(len(arg)==0):
            self.Point=Octpoint();
            self.poid = 0;
        elif(len(arg)==4):
            self.Point = Octpoint(arg[0],arg[1],arg[2])

            self.object = arg[3];
            self.poid = 1;
        elif (arg[0] > arg[3] or arg[1] > arg[4] or arg[2] > arg[5]):
                raise Exception("The bounds are not properly set!");
        else:

            self.poid = 0;
            self.children=[]
            self.Point = None;
            self.topLeftFront = Octpoint(arg[0], arg[1], arg[2]);
            self.bottomRightBack = Octpoint(arg[3],arg[4],arg[5]);
            for i in range (8):
                self.children.append(Octree())



#fonction pour chercher un point en fonction de ses coordonnées x y et z
    def find(self,x,y,z):

        if(x < self.topLeftFront.getX() or x > self.bottomRightBack.getX() or y < self.topLeftFront.getY() or y > self.bottomRightBack.getY() or z < self.topLeftFront.getZ() or z > self.bottomRightBack.getZ()):

            return False
        else:

            midx = (self.topLeftFront.getX() + self.bottomRightBack.getX()) / 2;

            midy = (self.topLeftFront.getY() + self.bottomRightBack.getY()) / 2;

            midz = (self.topLeftFront.getZ() + self.bottomRightBack.getZ()) / 2;
            pos=-1
            if (x <= midx):
                if (y <= midy):
                    if (z <= midz):
                        pos = 0;
                    else:
                        pos = 4;
                else:
                    if (z <= midz):
                        pos = 3;
                    else:
                        pos = 7;

            else:
                if (y <= midy):
                    if (z <= midz):
                        pos = 1;
                    else:
                        pos = 5;
                else :
                    if (z <= midz):
                        pos = 2;
                    else:
                        pos = 6;


            if (self.children[pos].Point == None):

                return self.children[pos].find(x, y, z);
            elif (self.children[pos].Point.isnullified()):

                return False;
            elif(x == self.children[pos].Point.getX() and y == self.children[pos].Point.getY() and z == self.children[pos].Point.getZ()):


                return True



#fonction qui insert un point dans notre octree
    def insert(self,x,y,z,object):



        if (self.find(x, y, z)):
            raise Exception("Point already exists in the tree. X");

        if (x < self.topLeftFront.getX() or x > self.bottomRightBack.getX() or y < self.topLeftFront.getY() or y > self.bottomRightBack.getY() or z < self.topLeftFront.getZ() or z > self.bottomRightBack.getZ()):

            raise Exception("Insertion point is out of bounds! X: " );

        midx = (self.topLeftFront.getX() + self.bottomRightBack.getX()) / 2;

        midy = (self.topLeftFront.getY() + self.bottomRightBack.getY()) / 2;

        midz = (self.topLeftFront.getZ() + self.bottomRightBack.getZ()) / 2;


        self.poid = self.poid + 1;

        if (x <= midx):
            if (y <= midy):
                if (z <= midz):
                    pos = 0;
                else:
                    pos = 4;
            else:
                if (z <= midz):
                    pos = 3;
                else:
                    pos = 7;

        else:
            if (y <= midy):
                if (z <= midz):
                    pos = 1;
                else:
                    pos = 5;
            else :
                if (z <= midz):
                    pos = 2;
                else:
                    pos = 6;

        if (self.children[pos].Point == None):
            self.children[pos].insert(x, y, z, object);
            return


        elif (self.children[pos].Point.isnullified()):
                self.children[pos] =None
                self.children[pos] =Octree(x, y, z, object);
                return
        else:


                x_ = self.children[pos].Point.getX();

                y_ = self.children[pos].Point.getY();

                z_ = self.children[pos].Point.getZ();

                object_ = self.children[pos].object;
                self.children[pos] = None

                if (pos == 0):
                    self.children[pos] = Octree(self.topLeftFront.getX(), self.topLeftFront.getY(), self.topLeftFront.getZ(), midx, midy, midz);

                elif (pos == 1):
                    self.children[pos] = Octree(midx +0.0000000000000000000000001, self.topLeftFront.getY(), self.topLeftFront.getZ(), self.bottomRightBack.getX(), midy, midz);

                elif (pos == 2):
                    self.children[pos] = Octree(midx+0.0000000000000000000000001, midy+0.0000000000000000000000001, self.topLeftFront.getZ(), self.bottomRightBack.getX(), self.bottomRightBack.getY(), midz);

                elif (pos == 3):
                    self.children[pos] = Octree(self.topLeftFront.getX(), midy +0.0000000000000000000000001, self.topLeftFront.getZ(), midx, self.bottomRightBack.getY(), midz);

                elif (pos == 4):

                    self.children[pos] = Octree(self.topLeftFront.getX(), self.topLeftFront.getY(), midz + +0.0000000000000000000000001, midx, midy, self.bottomRightBack.getZ());

                elif (pos == 5):
                    self.children[pos] =Octree(midx +0.0000000000000000000000001, self.topLeftFront.getY(), midz +0.0000000000000000000000001, self.bottomRightBack.getX(), midy, self.bottomRightBack.getZ());

                elif (pos == 6):
                    self.children[pos] =Octree(midx +0.0000000000000000000000001, midy +0.0000000000000000000000001, midz +0.0000000000000000000000001, self.bottomRightBack.getX(), self.bottomRightBack.getY(), self.bottomRightBack.getZ());

                elif (pos == 7):
                    self.children[pos] =Octree(self.topLeftFront.getX(), midy +0.0000000000000000000000001, midz +0.0000000000000000000000001, midx, self.bottomRightBack.getY(), self.bottomRightBack.getZ());


                self.children[pos].insert(x_, y_, z_, object_);

                self.children[pos].insert(x, y, z, object);
#fonction qui retourne le point aux coordonées x y et z
    def get(self,x,y,z):
        if (self.find(x, y, z)):

                midx = (self.topLeftFront.getX() + self.bottomRightBack.getX()) / 2;

                midy = (self.topLeftFront.getY() + self.bottomRightBack.getY()) / 2;

                midz = (self.topLeftFront.getZ() + self.bottomRightBack.getZ()) / 2;
                pos=-1
                if (x <= midx):
                    if (y <= midy):
                        if (z <= midz):
                            pos = 0;
                        else:
                            pos = 4;
                    else:
                        if (z <= midz):
                            pos = 3;
                        else:
                            pos = 7;

                else:
                    if (y <= midy):
                        if (z <= midz):
                            pos = 1;
                        else:
                            pos = 5;
                    else :
                        if (z <= midz):
                            pos = 2;
                        else:
                            pos = 6;


                if (self.children[pos].Point == None):

                    return self.children[pos].get(x, y, z);
                if (self.children[pos].Point.isnullified()):

                    return False;
                if(x == self.children[pos].Point.getX() and y == self.children[pos].Point.getY() and z == self.children[pos].Point.getZ()):


                    return self.children[pos].object
        else:
            return None

#fonction qui supprime un point de l'octree
    def remove(self,x,y,z):
        if (self.find(x, y, z)):

                midx = (self.topLeftFront.getX() + self.bottomRightBack.getX()) / 2;

                midy = (self.topLeftFront.getY() + self.bottomRightBack.getY()) / 2;

                midz = (self.topLeftFront.getZ() + self.bottomRightBack.getZ()) / 2;
                pos=-1
                if (x <= midx):
                    if (y <= midy):
                        if (z <= midz):
                            pos = 0;
                        else:
                            pos = 4;
                    else:
                        if (z <= midz):
                            pos = 3;
                        else:
                            pos = 7;

                else:
                    if (y <= midy):
                        if (z <= midz):
                            pos = 1;
                        else:
                            pos = 5;
                    else :
                        if (z <= midz):
                            pos = 2;
                        else:
                            pos = 6;

                self.poid=self.poid-1
                if (self.children[pos].Point == None):

                    return self.children[pos].remove(x, y, z);
                if (self.children[pos].Point.isnullified()):

                    return False;
                if(x == self.children[pos].Point.getX() and y == self.children[pos].Point.getY() and z == self.children[pos].Point.getZ()):

                    self.children[pos]=Octree()
        return




#fonction qui calcule le centre de gravité
    def calculate_center_of_mass(octree, p):

        o = Octpoint(0, 0, 0)

        if octree.Point is not None and not octree.Point.isnullified:
            o.setX(o.getX() + octree.Point.getX())
            o.setY(o.getY() + octree.Point.getY())
            o.setZ(o.getZ() + octree.Point.getZ())

        if hasattr(octree, 'children'):
            for child in octree.children:
                b = child.calculate_center_of_mass(child, 1)
                o.setX(o.getX() + b.getX())
                o.setY(o.getY() + b.getY())
                o.setZ(o.getZ() + b.getZ())

        o.setX(o.getX() / p)
        o.setY(o.getY() / p)
        o.setZ(o.getZ() / p)
        return o
